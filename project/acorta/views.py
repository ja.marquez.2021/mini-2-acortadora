from django.http import HttpResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from .models import Count
from .models import URL_to_short
from django.shortcuts import redirect

PAGE_QUEST = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Dont use "reset", is reserved to reset the URL list</p>
    <p>The form is:</p>
    <form action="/" method="POST">
      <p>URL \n</p>
      <input name="url" type="text" />
      <p>SHORT \n</p>
      <input name="short" type="text" />
      <p>\n</p>
    <input type="submit" value="Submit" />
    </form>
    <p>Your list of shorted url is: 
    {Dict_shorted_urls}</p>
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""
contador = Count.objects.first()


def dict_to_page():
    entradas = URL_to_short.objects.all()
    entradas_dict = {}
    for entrada in entradas:
        entradas_dict[entrada.clave] = entrada.valor
    dict_as_page = ""
    if entradas_dict:
        for clave, valor in entradas_dict.items():
            dict_as_page += f'<p>Shorted: {clave} -----> URL: {valor}</p>'
    return dict_as_page


@csrf_exempt
def handler1(request):
    if request.method == "GET":
        # Here we do nothing, is reserved if we want to implement something
        pass
    elif (request.method == "POST"
          and request.POST.get("url")
          and "short" in request.POST
          and not (URL_to_short.objects.filter(clave=request.POST.get("short")).exists() or
                   URL_to_short.objects.filter(valor=request.POST.get("url")).exists())):

        url = request.POST.get("url")
        if not (request.POST.get("url")[0:7] == "http://" or request.POST.get("url")[0:8] == "https://"):
            url = "https://" + url
        # this if is created to know if a short thas was introduces without "https://" is in the dattabase
        if not URL_to_short.objects.filter(valor=url).exists():
            if request.POST.get("short"):
                short = request.POST.get("short")
            else:
                contador.count += 1
                contador.save()
                short = contador.count

            nueva_url = URL_to_short(clave=short, valor=url)
            nueva_url.save()
    else:
        return HttpResponseBadRequest("Ocurrió un error en la solicitud.")
        print("someone try to do something weird")
    page = PAGE_QUEST.format(Dict_shorted_urls=dict_to_page())
    return HttpResponse(page)
                                             

def handler2(request, short):
    if request.method == "GET" and URL_to_short.objects.filter(clave=short).exists():
        page = redirect(URL_to_short.objects.filter(clave=short).first().valor)
    elif short == "reset":
        contador.count = 0
        contador.save()
        entradas = URL_to_short.objects.all()
        for entrada in entradas:
            print(f"Clave: {entrada.clave}, Valor: {entrada.valor}")
        URL_to_short.objects.all().delete()
        page = HttpResponse(f"Se han borrado todas las entradas anteriores, mas te vale no ser un hacker!")
    else:
        page = PAGE_NOT_FOUND.format(resource=short)
    return page
