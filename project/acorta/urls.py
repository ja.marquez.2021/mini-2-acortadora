from django.urls import path

from . import views

urlpatterns = [
    path('', views.handler1),
    path('<short>', views.handler2),
]
