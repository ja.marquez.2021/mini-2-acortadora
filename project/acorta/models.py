from django.db import models

class URL_to_short(models.Model):  # tabla contenido con dos campos (clave=short y valor=url)
    clave = models.CharField(max_length=1024)
    valor = models.CharField(max_length=1024)
    def __str__(self):
        return f"Clave: {self.clave}, Valor: {self.valor}"

class Count(models.Model):  # Por que numero de url sin short asignado voy
    count = models.IntegerField(unique=True, default=0)

    def __str__(self):
        return str(self.count)
